Summary: AlmaLinux Testing repository configuration
Name:    almalinux-release-testing
Version: 8
Release: 1%{?dist}
License: GPLv2
URL:     https://almalinux.org/
BuildArch: noarch
Source0: almalinux-testing.repo

Provides: almalinux-release-testing = 8

%description
DNF configuration for AlmaLinux Testing repo

%install
install -D -m 644 %{SOURCE0} %{buildroot}%{_sysconfdir}/yum.repos.d/almalinux-testing.repo

%files
%defattr(-,root,root)
%config(noreplace) %{_sysconfdir}/yum.repos.d/almalinux-testing.repo

%changelog
* Fri Jul 28 2023 Andrew Lukoshko <alukoshko@almalinux.org> - 8-1
- Initial version
